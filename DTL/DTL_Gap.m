function DTL_GAP = DTL_Gap(DTL, beta0, fieldmap)
    RF_Track;
    
    %% DTL energy gain
    DTL_dE = DTL.E0TL * cosd(DTL.Phid) / 1e6; % MeV

    if DTL_dE == 0
        DTL_GAP = Drift (DTL.L);
        return;
    end
    
    %% Standing-wave cell
    if fieldmap<39
        filename = [ 'Tank1/OUTSF7_2d_' num2str(fieldmap) '.TXT'];
    elseif fieldmap<39+42
        filename = [ 'Tank2/OUTSF7_' num2str(fieldmap-39) '.TXT'];
    else
        filename = [ 'Tank3/OUTSF7_' num2str(fieldmap-39-42) '.TXT'];
    end

    % remove empty strings
    system(['grep ''[[:alnum:]]'' ' filename ' > ' tmpname=tempname ]);

    % read file
    F = dlmread (tmpname, '', 2, 0);

    % remove temporary file
    delete(tmpname);

    hz = diff(unique(sort(F(:,1))))(1); % cm
    hr = diff(unique(sort(F(:,2))))(1); % cm

    ii = int32 (round ((F(:,1) - min (F(:,1))) / hz) + 1);
    jj = int32 (round ((F(:,2) - min (F(:,2))) / hr) + 1);

    for i=1:length(ii)
        Ez(ii(i), jj(i)) = F(i,3) * 1e6; % V/m
        Er(ii(i), jj(i)) = F(i,4) * 1e6; % V/m
    end

    hz /= 1e2;
    hr /= 1e2;
    
    if 0

        DTL_RF = struct;
    
        %% Reference particle
        mass = 939; % MeV/c^2
        P0 = mass * beta0 / sqrt(1-beta0*beta0); % MeV/c
        B0 = Bunch6d(mass, 0.0, +1, [ 0 0 0 0 0 P0 ]);
        
        function [dE,T] = get_dE_and_T(setup)
            scaling = setup.scaling %

            DTL_RF = RF_Track.RF_FieldMap_2d (scaling*Er, scaling*Ez, 0, 0, hr, hz, -1, DTL.freq, 1);
            DTL_RF.set_odeint_algorithm('leapfrog');
            DTL_RF.set_nsteps(200);
            
            DL = DTL.L - DTL_RF.get_length();
            
            DTL_GAP = RF_Track.Lattice();
            DTL_GAP.append (Drift (DL/2));
            DTL_GAP.append_ref (DTL_RF);
            DTL_GAP.append (Drift (DL/2));

            % on crest
            DTL_RF.set_phid(0.0);
            B1 = DTL_GAP.track(B0);
            dE_onCrest = B1.get_phase_space('%K') - B0.get_phase_space('%K')
            
            % synchromous phase
            DTL_RF.set_phid(DTL.Phid);
            B1 = DTL_GAP.track(B0);
            dE = B1.get_phase_space('%K') - B0.get_phase_space('%K')

            % compute the transit time factor
            N = 101;
            Z = linspace(0, DTL.L, N) * 1e3;
            O = zeros(1, N);
            
            DTL_RF.set_phid( 0.0); [Ereal,Br] = DTL_GAP.get_field(O, O, Z, O);
            DTL_RF.set_phid(90.0); [Eimag,Bi] = DTL_GAP.get_field(O, O, Z, O);
            Ef = hypot(Ereal(:,3), Eimag(:,3));
            
            dE_DC = trapz(Z, Ef) / 1e9; % MV
            
            T = dE_onCrest / dE_DC;
        end
        
        function F = constrain(X, a, b)
            F = a + (b-a) * (atand(X) + 90) / 180;
        end
        
        function setup = setup_from_X(X)
            setup.scaling = constrain(X(1), 0.99, 1.01); %
        end

        function M = merit(X)
            [dE,T] = get_dE_and_T ( setup_from_X (X) );
            M_dE = (dE / DTL_dE - 1)^2
            M_T  = ( T / DTL.T  - 1)^2
            M = M_dE + M_T;
        end

        X_min = [];
        F_min = Inf; 
        for i=1:3
            [X,F] = fminsearch(@merit, randn(1), optimset('TolFun', 1e-3));
            F
            if F<F_min
                F_min = F;
                X_min = X;
            end
        end

        merit(X_min)
        
        DTL_RF.unset_t0();
        DTL_RF.set_phid(DTL.Phid);
    
    else
        
        DTL_RF = RF_Track.RF_FieldMap_2d (Er, Ez, 0, 0, hr, hz, -1, DTL.freq, 1);
        DTL_RF.set_odeint_algorithm('rk2');
        DTL_RF.set_phid(DTL.Phid);
        DTL_RF.set_nsteps(200);
        
        DL = DTL.L - DTL_RF.get_length();
        
        DTL_GAP = RF_Track.Lattice();
        DTL_GAP.append (Drift (DL/2));
        DTL_GAP.append_ref (DTL_RF);
        DTL_GAP.append (Drift (DL/2));

    end
    
endfunction
