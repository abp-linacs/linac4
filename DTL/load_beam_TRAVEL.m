function [B0,P0] = load_beam_TRAVEL(filename, Q_pC)
    RF_Track;

    F = importdata (filename, ' ', 8);
    M = F.data;

    pos = strfind(F.textdata{3}, '!')-1;
    if isempty(pos)
        error('undefined reference momentum and frequency')
    end

    Pref = str2num(strtrim(strtrunc(F.textdata{3}, pos))) * 1e3 % MeV/c
    frequency = str2num(strtrim(strtrunc(F.textdata{5}, pos)))  % Hz

    x  = M(:,2) * 1e3; % mm
    xp = M(:,3) * 1e3; % mrad
    y  = M(:,4) * 1e3; % mm
    yp = M(:,5) * 1e3; % mrad
    t  = M(:,6) / (2*pi*frequency) * RF_Track.s; % mm/c
    P  = M(:,7)*Pref + Pref; % MeV/c
    Q  = M(:,9); % e
    m  = M(:,10) * 1e3; % MeV/c^2
    N  = ones(size(x)) * Q_pC * RF_Track.pC / length(x); % e

    B0 = Bunch6d ([ x xp y yp t P m Q N ] );
    P0 = Bunch6d ([ 0 0 0 0 mean(t) Pref m(1) Q(1) ]);

