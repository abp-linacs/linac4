RF_Track;

do_SC = 0;

Q_pC = 176.03634298693925; % pC, total bunch charge
                           % 176 pC * 352 MHz = 61.95 mA

Q_pC = 70.980381022685336; % pC, total bunch charge
                           % 25 mA / 352.21 MHz = 70.980381022685336 pC

% [B0,P0] = load_beam_TRAVEL ('~/linac4/Linac4/DTL_in.txt', Q_pC);
[B0,P0] = load_beam_TRAVEL ('~/linac4/Linac4/SP1.txt', Q_pC); % 1 particle

beta0_from_file = mean (B0.get_phase_space('%Vz'))

if 0
    beta0 = 0.080203; % nominal beta
    mass = 939; % MeV/c^2
    P0 = mass * beta0 / sqrt(1-beta0*beta0); % MeV/c
    B0 = Bunch6d(mass, 0.0, +1, [ 0 0 0 0 0 P0 ]);
end

load Tank1.dat

Tank1 = RF_FieldMap_1d (Tank1.Ez, Tank1.hz, -1, Tank1.freq, 1, 1, 1);
Tank1.set_phid(0.0);
Tank1.set_odeint_algorithm ('rk2');
Tank1.set_nsteps(10000);
Tank1.set_tt_nsteps(100);



DTL = Lattice();
DTL.append_ref (Tank1);
P_max = DTL.autophase(P0)

%{
if do_SC
    period = 851.1753158626955837; % mm/c
    B0.set_coasting (period);
    
    for E = DTL.get_rf_elements()
        E{1}.set_sc_nsteps(10);
    end
    
    for E = DTL.get_quadrupoles()
        E{1}.set_sc_nsteps(5);
    end
end
%}

%% START
if do_SC
    OUTDIR = 'results-SC-1fieldmap';
else
    OUTDIR = 'results-noSC-1fieldmap';
end
system([ 'mkdir -p ' OUTDIR ]);
cd(OUTDIR);

tic
P1 = DTL.track(P0);
B1 = DTL.track(B0);
toc

P1.save('reference_out.rft');
B1.save('bunch_out.rft');
B1.save_as_dst_file('bunch_out.dst', 352.2);

T = DTL.get_transport_table('%S %mean_P %mean_K');

figure(1)
plot (T(:,1), T(:,2))
xlabel('S (m)');
ylabel('P (MeV/c)');

figure(2)
plot (T(:,1), T(:,3))
xlabel('S (m)');
ylabel('K (MeV)');
xlim([ min(T(:,1)), max(T(:,1)) ]);
print -dpng plot_energy.png

figure(8);
Za = linspace(0, DTL.get_length(), 5000) * 1e3; % mm
O = zeros(1,5000);
[E,B] = DTL.get_field(O, O, Za, O);
plot(Za/1e3, E(:,3)/1e6)
xlabel('S [m]');
ylabel('E_z [MV/m]');

%% Transport table
T = DTL.get_transport_table('%S %mean_K %sigma_x %sigma_y %sigma_t %emitt_x %emitt_y %emitt_4d %emitt_z %emitt_6d %sigma_pt');
save -text transport_table.txt T

%% Plots
T = DTL.get_transport_table('%S %emitt_x %emitt_y %emitt_4d');
X = DTL.get_transport_table('%S %mean_x %mean_y');
S = DTL.get_transport_table('%S %sigma_x %sigma_y');

figure(1)
clf
plot(T(:,1), T(:,2), 'b-', T(:,1), T(:,3), 'r-', T(:,1), T(:,4), 'k-');
legend('emitt x', 'emitt y', 'emitt 4d', 'location', 'northwest');
xlabel('S [m]');
ylabel('normalized emitt [mm.mrad]');
print -dpng plot_DTL_emitt.png

figure(2)
clf
plot(S(:,1), S(:,2), 'b-', S(:,1), S(:,3), 'r-');
legend('sigma x', 'sigma y', 'location', 'northwest');
xlabel('S [m]');
ylabel('\sigma [mm]');
print -dpng plot_DTL_sigma.png

figure(4)
clf
hold on
scatter(B0.get_phase_space('%x'), B0.get_phase_space('%Px'), '.');
scatter(B1.get_phase_space('%x'), B1.get_phase_space('%Px'), '.');
title('initial and final horizontal phase space');
legend('initial', 'final');
xlabel('x [mm]');
ylabel('P_x [MeV/c]');
print -dpng plot_DTL_xPx.png

figure(5)
clf
hold on
scatter(B0.get_phase_space('%y'), B0.get_phase_space('%Py'), '.');
scatter(B1.get_phase_space('%y'), B1.get_phase_space('%Py'), '.');
title('initial and final vertical phase space');
legend('initial', 'final');
xlabel('y [mm]');
ylabel('P_y [MeV/c]');
print -dpng plot_DTL_yPy.png

figure(10)
clf
hold on
scatter(B1.get_phase_space('%deg@352.2'), B1.get_phase_space('%P'), '.');
title('initial and final vertical phase space');
xlabel('phi [deg]');
ylabel('P [MeV/c]');
print -dpng plot_DTL_phiP.png

figure(6)

M0 = B0.get_phase_space('%N %Q %t');

[H,X] = hist(M0(:,3), 16);

dt = X(2) - X(1); % mm/c

Q_pC = abs(B0.get_total_charge()) / RF_Track.pC;

H *= Q_pC / sum(H);
H *= 299.792458 / dt; % mA, pC / (mm/c) = 299.792458 mA

bar(X/RF_Track.ns, H, 'hist')
xlabel('t [ns]');
ylabel('I [mA]');

print -dpng plot_DTL_input_current.png

figure(7)

M0 = B1.get_phase_space('%N %Q %t');

[H,X] = hist(M0(:,3), 64);

dt = X(2) - X(1);

Q_pC = B1.get_total_charge() / RF_Track.pC;

H *= Q_pC / sum(H);

H *= 299.792458 / dt; % mA, pC / (mm/c) = 299.792458 mA

bar(X/RF_Track.ns,H,'hist')
xlabel('t [ns]');
ylabel('I [mA]');

print -dpng plot_DTL_output_current.png

%% END
cd('..')

beta0_initial = B0.get_phase_space('%Vz')
beta0_final = B1.get_phase_space('%Vz')