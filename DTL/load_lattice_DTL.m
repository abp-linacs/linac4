%% Read a TraeWin lattice file
function lattice = load_lattice_DTL(filename)
    RF_Track;
    freq = 0; % Hz
    lattice = Lattice();
    fid = fopen (filename);
    fieldmap = 0;
    if fid
        while !isscalar(txt = fgetl (fid)) || txt != -1
            cstr = strsplit(txt);
            card = cstr{1};
            if length(card) == 0 || card(1) == ';'
            elseif strcmp(card, 'FREQ')
                freq = str2num(cstr{2})*1e6; % Hz
            elseif strcmp(card, 'DRIFT')
                L = str2num(cstr{2}) / 1e3; % m
                aperture = str2num(cstr{3}) / 1e3; % m, aperture
                D = Drift(L);
                D.set_aperture(aperture, aperture, 'circular');
                lattice.append(D);
                printf('D = Drift(%g); ', L)
                printf('D.set_aperture(%g, %g, ''circular''); ', aperture, aperture);
                printf('lattice.append(D);\n');
            elseif strcmp(card, 'QUAD')
                L = str2num(cstr{2}) / 1e3; % m
                G = str2num(cstr{3}); % T/m, gradient
                aperture = str2num(cstr{4}) / 1e3; % m, aperture
                Q = Quadrupole(L);
                Q.set_gradient(G);
                Q.set_aperture(aperture, aperture, 'circular');
                lattice.append(Q);
                printf('Q = Quadrupole(%g); ', L)
                printf('Q.set_gradient(%g); ', G);
                printf('Q.set_aperture(%g, %g, ''circular''); ', aperture, aperture);
                printf('lattice.append(Q);\n');
            elseif strcmp(card, 'DTL_CEL')
                L = str2num(cstr{2}) / 1e3; % m
                LQ1 = str2num(cstr{3}) / 1e3; % m, first half quadrupole length
                LQ2 = str2num(cstr{4}) / 1e3; % m, second half quadrupole length
                gc = str2num(cstr{5}) / 1e3; % m, gap center shift
                G1 = str2num(cstr{6}); % T/m
                G2 = str2num(cstr{7}); % T/m
                E0TL = str2num(cstr{8}); % V
                phid = str2num(cstr{9}); % deg
                aperture = str2num(cstr{10}) / 1e3; % m
                P = str2num(cstr{11}); % reference phase
                beta0 = str2num(cstr{12}) % c, particle velocity
                T = str2num(cstr{13}); % transit time factor
                if T
                    E = E0TL / L / T; % V/m
                else
                    E = 0; % V/m
                end
                Lgap = L - LQ1 - LQ2; % m
                gap_position = L/2 - gc;
                % DTL GAP
                DTL.L = L; % m
                DTL.E0TL = E0TL; % V
                DTL.T = T;
                DTL.E = E; % V/m
                DTL.Phid = phid; % deg
                DTL.freq = freq; % Hz
                if E0TL>0
                    RF = DTL_Gap(DTL, beta0, fieldmap++);
                    LRF = RF.get_length();
                else
                    RF = Drift(L);
                    LRF = L;
                end
                %%
                SQ1 = 0.0; % m, start point of the first half quadrupole
                SQ2 = L - LQ2; % m, start point of the first half quadrupole
                SRF = gap_position - LRF/2;% start point of RF gap
                EQ1 = LQ1; % m, end point of the first half quadrupole
                EQ2 = L; % m, end point of the first half quadrupole
                ERF = gap_position + LRF/2; % end point of RF gap
                %%
                Q1 = Quadrupole(LQ1);
                Q1.set_gradient(G1);
                Q1.set_aperture(aperture, aperture, 'circular');
                Q2 = Quadrupole(LQ2);
                Q2.set_gradient(G2);
                Q2.set_aperture(aperture, aperture, 'circular');
                D1 = Drift(SRF - EQ1);
                D2 = Drift(SQ2 - ERF);
                lattice.append (Q1);
                lattice.append (D1);
                lattice.append (RF);
                lattice.append (D2);
                lattice.append (Q2);
            elseif strcmp(card, 'LATTICE')
            elseif strcmp(card, 'LATTICE_END')
            elseif strcmp(card, 'END')
            else
                disp(txt)
                warning([ 'unknown card ' card ]);
            end
        end
        fclose (fid);
    end
endfunction
