RF_Track;

DTL = load_lattice_DTL ('New_DTL.dat');
DTL.set_t0(0.0);

Za = 0:ceil(DTL.get_length()*1e3);
hz = 0.001;
frequency = 352.2e6; % Hz

[E,B] = DTL.get_field(0, 0, Za, 0);
Ez = E(:,3);

plot(Za/1e3, Ez/1e6)
xlabel('S [m]')
ylabel('E_z [MV/m]')
print -dpng plot_Tanks_Ez.png -S'1600,400' -F:12

% Separate the three tanks
tank1_idx = 1:find(Ez(1:4000), 1, 'last');
tank2_idx = 4001 + (find(Ez(4001:11600), 1, 'first') - 2):4001 + find(Ez(4001:11600), 1, 'last');
tank3_idx = 11601 + (find(Ez(11601:end), 1, 'first') - 2):11601 + find(Ez(11601:end), 1, 'last');

% Tank 1
Tank1.S = Za(tank1_idx(1))/1e3; % m
Tank1.Ez = Ez(tank1_idx);
Tank1.hz = hz;
Tank1.freq = frequency;
save -text Tank1.dat Tank1

% Tank 2
Tank2.S = Za(tank2_idx(1))/1e3; % m
Tank2.Ez = Ez(tank2_idx);
Tank2.hz = hz;
Tank2.freq = frequency;
save -text Tank2.dat Tank2

% Tank 3
Tank3.S = Za(tank3_idx(1))/1e3; % m
Tank3.Ez = Ez(tank3_idx);
Tank3.hz = hz;
Tank3.freq = frequency;
save -text Tank3.dat Tank3
