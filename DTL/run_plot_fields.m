RF_Track;

for i=0:38
    T = load_DTL_Gap ([ 'Tank1/OUTSF7_2d_' num2str(i) '.TXT']);
    T.set_t0(0.0);
    L = T.get_length()
    Z = linspace(0, L*1e3, 201); % mm
    X = linspace(-10, 10, 101); % mm

    [XX,ZZ] = meshgrid (X, Z); % mm

    [E,B] = T.get_field (XX(:), 0, ZZ(:), 0);

    Ex = reshape(E(:,1), size(XX));
    Ey = reshape(E(:,2), size(XX));
    Ez = reshape(E(:,3), size(XX));
    T.unset_t0();

    figure(i+1);
    subplot(1,2,1)
    pcolor(X, Z, Ex)
    shading flat
    title('E_x')
    xlabel('X [mm]')
    ylabel('Z [mm]')

    subplot(1,2,2)
    pcolor(X, Z, Ez)
    shading flat
    title('E_z')
    xlabel('X [mm]')
    ylabel('Z [mm]')

end
