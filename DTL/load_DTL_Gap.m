function T = load_DTL_Gap(filename, scaling=1)

    RF_Track;

    % remove empty strings
    system(['grep ''[[:alnum:]]'' ' filename ' > ' tmpname=tempname ]);

    % read file
    F = dlmread (tmpname, '', 2, 0);

    % remove temporary file
    delete(tmpname);

    hz = diff(unique(sort(F(:,1))))(1); % cm
    hr = diff(unique(sort(F(:,2))))(1); % cm

    ii = int32 (round ((F(:,1) - min (F(:,1))) / hz) + 1);
    jj = int32 (round ((F(:,2) - min (F(:,2))) / hr) + 1);

    for i=1:length(ii)
        Ez(ii(i), jj(i)) = scaling * F(i,3) * 1e6; % V/m
        Er(ii(i), jj(i)) = scaling * F(i,4) * 1e6; % V/m
    end

    freq = 352.2e6; % Hz
    T = RF_FieldMap_2d_CINT (Er, Ez, 0, 0, hr/1e2, hz/1e2, -1, freq, 0);
