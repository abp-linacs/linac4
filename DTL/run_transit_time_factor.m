RF_Track;

fieldmap = 0;
beta0 = 0.0802028;

%% Reference particle
mass = 939; % MeV/c^2
P0 = mass * beta0 / sqrt(1-beta0*beta0); % MeV/c
B0 = Bunch6d(mass, 0.0, +1, [ 0 0 0 0 0 P0 ]);

DTL.L = 0.06891;
DTL.E0TL = 160165;
DTL.T = 0.75326;
DTL.E = 3085605.942361701;
DTL.Phid = -35;
DTL.freq = 352200000;

% remove empty strings
filename = [ 'Tank1/OUTSF7_2d_' num2str(fieldmap) '.TXT']
system(['grep ''[[:alnum:]]'' ' filename ' > ' tmpname=tempname ]);

% read file
F = dlmread (tmpname, '', 2, 0);

% remove temporary file
delete(tmpname);

%
hz = diff(unique(sort(F(:,1))))(1); % cm
hr = diff(unique(sort(F(:,2))))(1); % cm

ii = int32 (round ((F(:,1) - min (F(:,1))) / hz) + 1);
jj = int32 (round ((F(:,2) - min (F(:,2))) / hr) + 1);

for i=1:length(ii)
    Ez(ii(i), jj(i)) = F(i,3) * 1e6; % V/m
    Er(ii(i), jj(i)) = F(i,4) * 1e6; % V/m
end

hz /= 1e2;
hr /= 1e2;

DTL_RF = struct;

scaling = 1.0; %

DTL_RF = RF_Track.RF_FieldMap_2d (scaling*Er, scaling*Ez, 0, 0, hr, hz, -1, DTL.freq, +1);
DTL_RF.set_odeint_algorithm('rk2');
DTL_RF.set_odeint_epsabs(1e-6);
DTL_RF.set_nsteps(1000);

DL = DTL.L - DTL_RF.get_length();

Lat = RF_Track.Lattice();
Lat.append (Drift (DL/2));
Lat.append_ref (DTL_RF);
Lat.append (Drift (DL/2));

% on crest
DTL_RF.set_phid(0.0);
B1 = Lat.track(B0);
dE_onCrest = B1.get_phase_space('%K') - B0.get_phase_space('%K')

% synchromous phase
DTL_RF.set_phid(DTL.Phid);
B1 = Lat.track(B0);
dE = B1.get_phase_space('%K') - B0.get_phase_space('%K')

% compute the transit time factor
N = 1001;
Z = linspace(0, DTL.L, N) * 1e3;
O = zeros(1, N);

DTL_RF.set_phid( 0.0); [Ereal,~] = Lat.get_field(O, O, Z, O);
DTL_RF.set_phid(90.0); [Eimag,~] = Lat.get_field(O, O, Z, O);
Eabs = hypot(Ereal(:,3), Eimag(:,3));

dE_DC = trapz(Z, Eabs) / 1e9; % MV

T = dE_onCrest / dE_DC
